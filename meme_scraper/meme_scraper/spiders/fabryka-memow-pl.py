import scrapy

from meme_scraper import items
from meme_scraper import utils


class FabrykaMemowPL(scrapy.Spider):
    name = 'fabryka-memow-pl'

    start_urls = [
        'https://fabrykamemow.pl',
    ]

    def parse(self, response):
        # Get memes from current site
        entries = response.css(
            'div.pic.lbox.clearfix'
        )

        for meme_entry in entries:
            yield self._parse_meme_info(meme_entry)

        next_site_url = utils.get_value(
            response,
            '.list_next_page_button_wrapper a::attr(href)'
        )

        if next_site_url:
            yield scrapy.Request(next_site_url, callback=self.parse)

    @staticmethod
    def _parse_meme_info(entry):
        figure = entry.css('figure')

        url = utils.get_value(figure, 'a::attr(href)')
        img_url = utils.get_value(figure, 'img::attr(src)')
        img_alt = utils.get_value(figure, 'img::attr(alt)') \
            .split('\xe2\x80\x93')[1]

        template = utils.get_value(entry, '.stencil_info a *::text')
        datetime = utils.get_value(entry, 'time::attr(datetime)')
        author = utils.get_value(entry, 'address.author *::text')[13:]

        return items.FabrykaMemowPLMemeItem(
            url=url, template=template, img_url=img_url, img_alt=img_alt,
            datetime=datetime, author=author
        )
