# meme_scraper

This scaper was created using the Scrapy framework. In order to use it, you 
should either install Scrapy locally or use the Docker image.

```bash
docker pull vimagick/scrapyd
cd /path/to/the/meme-language/repository
docker run -it -v ${PWD}:/project vimagick/scrapyd bash
``` 

Then inside the container:
```bash
$ cd /project/meme_scraper
$ scrapy crawl memy-pl -o file.json
```