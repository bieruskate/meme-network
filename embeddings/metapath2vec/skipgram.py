import pickle
import numpy as np
from gensim.models import Word2Vec


def skipgram(walks, size, window, epochs):
    model = Word2Vec(
        sentences=walks,
        size=size,
        window=window,
        sg=1,
        workers=8 # for now
    )

    model.train(
        sentences=walks,
        total_examples=len(walks),
        epochs=epochs
    )

    return model


def prepare_walks(raw_walks):
    walks = []
    for walks_for_node in raw_walks:
        for walk in walks_for_node:
            walks.append(list(map(str, walk)))
    return walks


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Meme graph metapath2vec - skipgram part')
    parser.add_argument('rw_pickle_path', type=str)
    parser.add_argument('model_save_path', type=str)

    args = parser.parse_args()

    with open(args.rw_pickle_path, 'rb') as f:
        raw_walks = pickle.load(f)

    walks = prepare_walks(raw_walks)

    model = skipgram(walks, 100, 5, 50)
    model.save(args.model_save_path)
