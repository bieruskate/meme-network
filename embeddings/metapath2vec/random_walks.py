import pickle
import json
import random
from tqdm import tqdm
import graph_tool.all as gt

from multiprocessing import Pool


def _meme_graph_loader(filepath):
    with open(filepath, 'rb') as f:
        return pickle.load(f)

def _meta_scheme_gen(meta_scheme_info):
    idx = 0
    repeat_type = None
    repeat_count = None

    while True:
        info = meta_scheme_info[idx]
        current_type = info['type']
        
        possible_types = [current_type]
        if repeat_type and repeat_count:
            possible_types.append(repeat_type)
        yield possible_types

        actual_type = yield

        if actual_type == repeat_type:
            repeat_count -= 1
            continue

        idx = (idx + 1) % len(meta_scheme_info)
        if 'repeat' in info:
            repeat_type = current_type
            repeat_count = info['repeat']
        else:
            repeat_type, repeat_count = None, None



def _random_walk_gen(g, start_node_id, walk_len, meta_scheme_info, skip_types):
    current_node = g.vertex(start_node_id)
    current_walk_len = 0
    meta_scheme_gen = _meta_scheme_gen(meta_scheme_info)

    assert g.vp.vertex_type[current_node] == next(meta_scheme_gen)[0]
    next(meta_scheme_gen)

    while current_walk_len < walk_len:
        current_type = g.vp.vertex_type[current_node]

        if current_type not in skip_types:
            current_walk_len += 1
            yield int(current_node)

        possible_next_types = meta_scheme_gen.send(current_type)
        possible_next_nodes = [n for n in current_node.all_neighbors()
                               if g.vp.vertex_type[n] in possible_next_types]

        if not possible_next_nodes:
            break

        current_node = random.choice(possible_next_nodes)
        next(meta_scheme_gen)
        

def generate_random_walks(g, start_node, num_walks, walk_len, meta_scheme_info,
                          main_type, skip_types):
    random_walks = []
    for _ in range(num_walks):
        walker = _random_walk_gen(g, start_node, walk_len, meta_scheme_info,
                                skip_types)
        walks = list(walker)
        random_walks.append(walks)
    return random_walks


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Meme graph metapath2vec - random walks part')
    parser.add_argument('meme_graph_pickle_path', type=str)
    parser.add_argument('rw_save_path', type=str)

    args = parser.parse_args()

    meme_graph = _meme_graph_loader(args.meme_graph_pickle_path)
    meta_scheme = [
        {'type': 'meme'},
        {'type': 'comment', 'repeat': 5},
        {'type': 'user'},
        {'type': 'comment', 'repeat': 5}
    ]

    start_nodes = [int(v) for v in meme_graph.vertices() if meme_graph.vp.vertex_type[v] == 'meme']

    def rw(start_node):
        return generate_random_walks(meme_graph, start_node, 5, 10, meta_scheme, 'meme', ['comment'])

    with Pool() as pool:
        walks = pool.map(rw, start_nodes)

    with open(args.rw_save_path, 'wb') as f:
        pickle.dump(walks, f)
    # with open('imgflip_rw.json', 'w') as f:
    #     json.dump(walks, f)
